`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    07:20:11 11/30/2017 
// Design Name: 
// Module Name:    rj45tousbuart_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rj45tousbuart_top(
    input rx_clk,
    output pmod_jb_2,
	 input rx_en,
    input [3:0] rx_data
    );

wire new_byte;
wire[7:0] fifo_byte;
wire tx_ready;



wire fifo_write;
wire fifo_full;

reg[3:0] low_nibble; always @(posedge rx_clk) begin if (rx_en) low_nibble<=rx_data; end

reg[3:0] high_nibble; always@(negedge rx_clk) begin if (rx_en) high_nibble<=rx_data; end

wire[7:0] fifo_in; assign fifo_in[7:4] = high_nibble[3:0];assign fifo_in[3:0]=low_nibble[3:0] ;

reg delayedrx_en; always @(posedge rx_clk) delayedrx_en<=rx_en;

assign fifo_write = delayedrx_en & ~fifo_full;

wire fifo_empty; 

reg fifo_read; always @(posedge rx_clk) begin if (fifo_read) fifo_read<=0; else fifo_read<=tx_ready & ~fifo_empty; end

FIFO fifo1(.clk(rx_clk), .din(fifo_in),.wr_en(fifo_write),.full(fifo_full),.dout(fifo_byte),.rd_en(fifo_read), .empty(fifo_empty));

uart_transmitter uart1(.clk(rx_clk),.uart_tx(pmod_jb_2),.rx_new_byte(fifo_read),.rx_byte(fifo_byte),.tx_ready(tx_ready));

endmodule
